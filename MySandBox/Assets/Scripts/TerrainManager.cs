﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class TerrainManager : MonoBehaviour {

    [SerializeField] private Terrain _terrain;

    public int terrainWitdh;
    public int terrainHeight;

    public float[,] actualTerrain;

    // ---------

    public GameObject esfera;
    public GameObject invisivel;

    public string filePath;

    // Use this for initialization
    void Start()
    {
        terrainWitdh = _terrain.terrainData.heightmapWidth;
        terrainHeight = _terrain.terrainData.heightmapHeight;

        actualTerrain = _terrain.terrainData.GetHeights(0, 0, terrainWitdh, terrainHeight);

        MakeFlat();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CarregarHeightMap(filePath, _terrain.terrainData);
        }

        if (Input.GetKey(KeyCode.I))
        {
            Instantiate(esfera, invisivel.transform.position, Quaternion.identity);
        }
    }

    public void MakeFlat()
    {

        float[,] newHeight = _terrain.terrainData.GetHeights(0, 0, terrainWitdh, terrainHeight);

        for (int y = 0; y < terrainHeight; y++)
        {
            for (int x = 0; x < terrainWitdh; x++)
            {
                newHeight[x, y] = 0;
            }
        }

        _terrain.terrainData.SetHeights(0, 0, newHeight);
    }

    public void ModifyTerrain(Vector3 clickPoint, int diametro)
    {
        int posX = (int)((clickPoint.x / _terrain.terrainData.size.x) * terrainWitdh);
        int posY = (int)((clickPoint.z / _terrain.terrainData.size.z) * terrainHeight);

        float[,] heightChange = new float[diametro, diametro];

        int raio = (int)(diametro / 2);

        for (int x = 0; x < diametro; x++)
        {
            for (int y = 0; y < diametro; y++)
            {
                int x2 = x - raio;
                int y2 = y - raio;

                float distanciaQuadrada = (x2 * x2) + (y2 * y2);

                heightChange[y, x] = actualTerrain[posY + y2, posX + x2] + (0.003f / (Mathf.Sqrt(distanciaQuadrada)));
                actualTerrain[posY + y2, posX + x2] = heightChange[y, x];
            }
        }

        _terrain.terrainData.SetHeights(posX - raio, posY - raio, heightChange);
    }

    public void CarregarHeightMap(string aFileName, TerrainData tData)
    {
        aFileName = Application.dataPath + aFileName;

        byte[] image = File.ReadAllBytes(aFileName);

        Texture2D heightmap = new Texture2D(2, 2);
        heightmap.LoadImage(image);

        Color[] values = heightmap.GetPixels();
        
        float[,] data = new float[heightmap.height, heightmap.width];

        int index = 0;
        for (int z = 0; z < heightmap.height; z++)
        {
            for (int x = 0; x < heightmap.width; x++)
            {
                data[z, x] = values[index].r;
                index++;
            }
        }

        tData.SetHeights(0, 0, data);
        
    }

}

