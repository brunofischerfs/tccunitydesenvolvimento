﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorManager : MonoBehaviour
{

    [SerializeField] private Terrain _terrain;
    [SerializeField] private TrailRenderer trailRenderer;
    [SerializeField] private GameObject _estrada;
    [SerializeField] private GameObject _aguaQuadradaPrefab;
    [SerializeField] private GameObject _aguaCircularPrefab;


    private UIManager _uiManager;
    private TerrainManager _terrainManager;
    private TrailRenderer trailRendererInstancia;

    public GameObject objSelecionado;
    public bool posicionandoObjeto;
    public bool editandoObjeto;
    public bool modificandoTerreno;
    public bool colisaoObjeto;
    public bool desenhandoEstrada;
    public bool posicionandoAgua;
    public Vector3 movimentoMouse;

    // Aux
    public string tipoAgua;

    // Use this for initialization
    void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _terrainManager = GameObject.Find("TerrainManager").GetComponent<TerrainManager>();

        posicionandoObjeto = false;
        editandoObjeto = false;
        modificandoTerreno = false;
        colisaoObjeto = false;
        desenhandoEstrada = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (objSelecionado != null)
        {
            if (posicionandoObjeto == true)
            {
                ModoPosicionarObj();
            }

            if (editandoObjeto == true)
            {
                ModoEditarObj();
            }
        }

        if (objSelecionado == null)
        {
            if (modificandoTerreno == true)
            {
                ModoModificarTerreno();

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    modificandoTerreno = false;
                    Debug.Log("Você saiu deste modo.");
                }
            }

            if (desenhandoEstrada == true)
            {
                ModoDesenharEstrada();

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    desenhandoEstrada = false;
                    Debug.Log("Você saiu deste modo.");
                }
            }

            if (posicionandoAgua == true)
            {
                ModoPosicionarAgua();
            }
        }
    }

    public void SelecionaObjeto(GameObject obj)
    {
        _uiManager.EsconderInfo();
        objSelecionado = Instantiate(obj, Vector3.zero, Quaternion.identity) as GameObject;
        posicionandoObjeto = true;
    }

    public void ColocaObjeto()
    {
        if (colisaoObjeto == false)
        {
            posicionandoObjeto = false;
            _uiManager.MostrarInfo("objeto");
            editandoObjeto = true;
        }
        else
        {
            posicionandoObjeto = false;
            Destroy(objSelecionado);
            Debug.Log("Não é possível colocar neste local.");
        }
    }

    public void Escalar(float valor)
    {
        if (valor >= objSelecionado.GetComponent<Objeto>().mEscala)
        {
            float valorAux = valor * 0.05f;
            objSelecionado.transform.localScale += new Vector3(valorAux, valorAux, valorAux);
        }
        else
        {
            float valorAux = valor * 0.05f;
            objSelecionado.transform.localScale -= new Vector3(valorAux, valorAux, valorAux);
        }
        objSelecionado.GetComponent<Objeto>().mEscala = valor;
    }

    public void Rotacionar(float valor)
    {
        if (valor < objSelecionado.GetComponent<Objeto>().mRotacao)
        {
            objSelecionado.transform.Rotate(0, valor * 7, 0, Space.Self);
        }
        else
        {
            objSelecionado.transform.Rotate(0, valor * (-7), 0, Space.Self);
        }
        objSelecionado.GetComponent<Objeto>().mRotacao = valor;
    }

    public void RegularNivelAgua(float valor)
    {
        if (valor >= objSelecionado.GetComponent<Objeto>().mNivelAgua)
        {
            objSelecionado.transform.position += new Vector3(0, 2, 0);
        }
        else
        {
            objSelecionado.transform.position -= new Vector3(0, 2, 0);
        }
        objSelecionado.GetComponent<Objeto>().mNivelAgua = valor;
    }

    private void ModoPosicionarObj()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Terreno")
            {
                if(objSelecionado.tag == "Agua")
                {
                    movimentoMouse.Set(hit.point.x, objSelecionado.transform.position.y , hit.point.z);
                }
                else if(objSelecionado.tag == "Vegetacao" || objSelecionado.tag == "Edificio")
                {
                    movimentoMouse.Set(hit.point.x, hit.point.y, hit.point.z);
                }
                objSelecionado.transform.position = movimentoMouse;
            }
        }
    }

    private void ModoEditarObj()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject != objSelecionado.gameObject)
                {
                    objSelecionado = null;
                    _uiManager.EsconderInfo();
                    editandoObjeto = false;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Delete))
        {
            Destroy(objSelecionado.gameObject);
            objSelecionado = null;
            _uiManager.EsconderInfo();
            editandoObjeto = false;
        }
    }

    private void ModoModificarTerreno()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                _terrainManager.ModifyTerrain(hit.point, 20);
            }
        }
    }

    private void ModoDesenharEstrada()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButton(0) && hit.collider.tag == "Terreno")
            {
                if (trailRendererInstancia == null)
                {
                    trailRendererInstancia = Instantiate(trailRenderer, hit.point + new Vector3(0, 0.1f, 0), Quaternion.identity) as TrailRenderer;
                }
                else
                {
                    movimentoMouse.Set(hit.point.x, hit.point.y + 0.1f, hit.point.z);
                    trailRendererInstancia.transform.position = movimentoMouse;
                }
            }
            if (Input.GetMouseButtonUp(0) && trailRendererInstancia != null)
            {
                _estrada = Instantiate(_estrada) as GameObject;
                trailRendererInstancia.BakeMesh(_estrada.GetComponent<MeshFilter>().mesh);

                Vector3[] vertices = _estrada.GetComponent<MeshFilter>().mesh.vertices;

                for (int i = 0; i < vertices.Length; i++)
                {
                    vertices[i] = new Vector3(vertices[i].x, _terrain.transform.position.y + 0.1f, vertices[i].z);
                }

                _estrada.GetComponent<MeshFilter>().mesh.vertices = vertices;

                Destroy(trailRendererInstancia);
            }
        }
    }

    private void ModoPosicionarAgua()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "Terreno")
                {
                    if(tipoAgua == "AguaCircular")
                    {
                        objSelecionado = Instantiate(_aguaCircularPrefab, hit.point + new Vector3(0, 3, 0), Quaternion.identity) as GameObject;
                    }
                    else if(tipoAgua == "AguaQuadrada")
                    {
                        objSelecionado = Instantiate(_aguaQuadradaPrefab, hit.point + new Vector3(0, 3, 0), Quaternion.identity) as GameObject;
                    }
                    _uiManager.MostrarInfo("agua");
                    posicionandoAgua = false;
                    editandoObjeto = true;
                }
            }
        }
    }
}
