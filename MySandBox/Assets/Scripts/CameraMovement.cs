﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    private float _speed;

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float _yaw = 0.0f;
    private float _pitch = 0.0f;

    public float startTime;
    public float endTime;

    // Use this for initialization
    void Start ()
    {
        _speed = 50.0f;
        speedH = 2.0f;
        speedV = 2.0f;
        _yaw = 0.0f;
        _pitch = 0.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        _yaw += speedH * Input.GetAxis("Mouse X");
        _pitch -= speedV * Input.GetAxis("Mouse Y");
        float translationZ = Input.GetAxis("Vertical");
        float translationX = Input.GetAxis("Horizontal");
        transform.Translate(translationX * _speed * Time.deltaTime, 0.0f, 0.0f);
        transform.Translate(0.0f, 0.0f, translationZ * _speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            transform.eulerAngles = new Vector3(_pitch, _yaw, 0.0f);
        }
    }
}
