﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public GameObject arvoreFir;

    private EditorManager _editorManager;

    public Image background;

    void Start()
    {
        _editorManager = GameObject.Find("EditorManager").GetComponent<EditorManager>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _editorManager.modificandoTerreno = false;
        background.fillCenter = true;
        _editorManager.SelecionaObjeto(arvoreFir);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        background.fillCenter = false;
        _editorManager.ColocaObjeto();
    }

}
