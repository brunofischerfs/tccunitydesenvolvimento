﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objeto : MonoBehaviour {

    private EditorManager _editorManager;
    private UIManager _uiManager;
    private Color corOriginal;

    public string nome;

    public float mRotacao; // multiplicador de rotacao
    public float mEscala; // multiplicador de escala
    public float mNivelAgua;

    // variveis auxiliares
    public float startTime;
    public float endTime;

    // Use this for initialization
    void Start()
    {
        _editorManager = GameObject.Find("EditorManager").GetComponent<EditorManager>();
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        mRotacao = 0.5f;
        mEscala = 0.5f;
        mNivelAgua = 0.5f;
        if (this.tag == "Vegetacao" || this.tag == "Edificio")
            corOriginal = this.GetComponent<Renderer>().material.color;
    }

    private void OnMouseDown()
    {
        _editorManager.objSelecionado = this.gameObject;
        _editorManager.editandoObjeto = true;
        if(this.tag == "Vegetacao" || this.tag == "Edificio")
        {
            _uiManager.MostrarInfo("objeto");
        }
        else if(this.tag == "Agua")
        {
            _uiManager.MostrarInfo("agua");
        }
        startTime = Time.time;
        endTime = startTime + 1.0f;
    }

    private void OnMouseDrag()
    {
        if (Time.time > endTime)
        {
            _editorManager.posicionandoObjeto = true;
            if(this.tag == "Vegetacao" || this.tag == "Edificio")
            {
                _uiManager.AtualizarObjXY((int)transform.position.x, (int)transform.position.z);
            }
        }
    }

    private void OnMouseUp()
    {
        _editorManager.posicionandoObjeto = false;
    }

    /*private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Edificio")
        {
            _editorManager.colisaoObjeto = true;
            _editorManager.objSelecionado.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (other.gameObject.tag == "Vegetacao")
        {
            _editorManager.colisaoObjeto = true;
            _editorManager.objSelecionado.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (other.gameObject.tag == "Estrada")
        {
            _editorManager.colisaoObjeto = true;
            _editorManager.objSelecionado.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    private void OnCollisionExit(Collision other)
    {
        this.GetComponent<Renderer>().material.color = corOriginal;
        _editorManager.colisaoObjeto = false;
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Edificio")
        {
            _editorManager.colisaoObjeto = true;
            _editorManager.objSelecionado.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (other.tag == "Vegetacao")
        {
            _editorManager.colisaoObjeto = true;
            _editorManager.objSelecionado.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (other.tag == "Estrada")
        {
            _editorManager.colisaoObjeto = true;
            _editorManager.objSelecionado.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        this.GetComponent<Renderer>().material.color = corOriginal;
        _editorManager.colisaoObjeto = false;
    }
}
