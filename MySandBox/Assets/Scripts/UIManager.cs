﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour {
    
    // Buttons
    public Button btnElevarTerreno;
    public Button btnDesenharEstrada;
    public Button btnAguaCircular;
    public Button btnAguaQuadrada;

    // Pane Object Info
    public GameObject panelObjInfo;
    public Text textObjNome;
    public Text textObjX;
    public Text textObjY;
    public Text textRotacao;
    public Text textEscala;
    public Slider sliderObjRotacao;
    public Slider sliderObjEscala;
    public Image imageObj;

    //Pane Water Info
    public GameObject panelWaterInfo;
    public Text textWaterNome;
    public InputField ifLarg;
    public InputField ifComp;
    public Text textNivelAgua;
    public Slider sliderNivelAgua;
    public Image imageWater;
    public Button btnAplicar;

    //  Booleans
    public bool preenchendoDados;

    // Scripts Reference
    private EditorManager _editorManager;

    // Aux Vars
    public GenerateMesh mymesh;
    public float ultimoValor = 0;

    // Use this for initialization
    void Start ()
    {
        _editorManager = GameObject.Find("EditorManager").GetComponent<EditorManager>();

        btnElevarTerreno.onClick.AddListener(delegate { ElevarTerreno(); });
        btnDesenharEstrada.onClick.AddListener(delegate { DesenharEstrada(); });
        btnAguaCircular.onClick.AddListener(delegate { ColocarAguaCircular(); });
        btnAguaQuadrada.onClick.AddListener(delegate { ColocarAguaQuadrada(); });
        btnAplicar.onClick.AddListener(delegate { AplicarMudancas(); });
	}

    private void DesenharEstrada()
    {
        EsconderInfo();
        _editorManager.objSelecionado = null;
        _editorManager.desenhandoEstrada = true;
    }

    private void ElevarTerreno()
    {
        EsconderInfo();
        _editorManager.objSelecionado = null;
        _editorManager.modificandoTerreno = true;
    }

    private void ColocarAguaQuadrada()
    {
        EsconderInfo();
        _editorManager.tipoAgua = "AguaQuadrada";
        _editorManager.posicionandoAgua = true;
    }

    private void ColocarAguaCircular()
    {
        EsconderInfo();
        _editorManager.tipoAgua = "AguaCircular";
        _editorManager.posicionandoAgua = true;
    }

    public void AtualizarObjXY(int x, int y)
    {
        textObjX.text = "X: " + x;
        textObjY.text = "Y: " + y;
    }

    private void PreencherObjInfo(string tipo)
    {
        preenchendoDados = true;

        if(tipo == "objeto")
        {
            textObjNome.text = "Nome: " + _editorManager.objSelecionado.GetComponent<Objeto>().nome;
            textObjX.text = "X: " + (int)_editorManager.objSelecionado.transform.position.x;
            textObjY.text = "Y: " + (int)_editorManager.objSelecionado.transform.position.z;
            sliderObjEscala.value = _editorManager.objSelecionado.GetComponent<Objeto>().mEscala;
            sliderObjRotacao.value = _editorManager.objSelecionado.GetComponent<Objeto>().mRotacao;
            textEscala.text = "Escala: " + (int)(sliderObjEscala.value * 10);
            textRotacao.text = "Rotação: " + (int)(sliderObjRotacao.value * 10);
            // Falta imagem
        }
        else if(tipo == "agua")
        {
            switch (_editorManager.tipoAgua)
            {
                case "AguaCircular":
                    textWaterNome.text = "Nome: " + _editorManager.objSelecionado.GetComponent<Objeto>().nome;
                    ifLarg.text = _editorManager.objSelecionado.transform.localScale.x.ToString();
                    ifComp.text = _editorManager.objSelecionado.transform.localScale.z.ToString();
                    sliderNivelAgua.value = _editorManager.objSelecionado.GetComponent<Objeto>().mNivelAgua;
                    textNivelAgua.text = "Nível da Água: " + (int)(sliderNivelAgua.value * 10);
                    break;
                case "AguaQuadrada":
                    textWaterNome.text = "Nome: " + _editorManager.objSelecionado.GetComponent<Objeto>().nome;
                    ifLarg.text = _editorManager.objSelecionado.GetComponent<GenerateMesh>().xSize.ToString();
                    ifComp.text = _editorManager.objSelecionado.GetComponent<GenerateMesh>().zSize.ToString();
                    sliderNivelAgua.value = _editorManager.objSelecionado.GetComponent<Objeto>().mNivelAgua;
                    textNivelAgua.text = "Nível da Água: " + (int)(sliderNivelAgua.value * 10);
                    break;
            }
            // Falta imagem
        }
        preenchendoDados = false;
    }

    public void MostrarInfo(string tipo)
    {
        if(tipo == "objeto")
        {
            PreencherObjInfo(tipo);
            panelObjInfo.SetActive(true);
        }
        else if(tipo == "agua")
        {
            PreencherObjInfo("agua");
            panelWaterInfo.SetActive(true);
        }
    }

    public void EsconderInfo()
    {
        panelObjInfo.SetActive(false);
        panelWaterInfo.SetActive(false);
    }

    public void ModificarEscala(float valor)
    {
        if(preenchendoDados == false)
        {
            textEscala.text = "Escala: " + (int)(sliderObjEscala.value * 10);
            _editorManager.Escalar(valor);
        }
    }

    public void ModificarRotacao(float valor)
    {
        if (preenchendoDados == false)
        {
            textRotacao.text = "Rotação: " + (int)(sliderObjRotacao.value * 10);
            _editorManager.Rotacionar(valor);
        }
    }

    public void ModificarNivelAgua(float valor)
    {
        if (preenchendoDados == false)
        {
            textNivelAgua.text = "Nível da Água: " + (int)(sliderNivelAgua.value * 10);
            _editorManager.RegularNivelAgua(valor);
        }
    }

    public void AplicarMudancas()
    {
        int x = int.Parse(ifLarg.text);
        int z = int.Parse(ifComp.text);
        GameObject obj = _editorManager.objSelecionado;
        if (_editorManager.tipoAgua == "AguaQuadrada")
        {
            obj.GetComponent<GenerateMesh>().xSize = x;
            obj.GetComponent<GenerateMesh>().zSize = z;
            obj.GetComponent<GenerateMesh>().CreateShape();
            obj.GetComponent<GenerateMesh>().UptadeMesh();
        }
        else if (_editorManager.tipoAgua == "AguaCircular")
        {
            obj.transform.localScale = new Vector3(x, obj.transform.position.y, z);
        }
        
    }
}
